<?php

class Converter
{
    private $number;
    private $result;
    private $map = [
        'M' => 1000,
        'CM' => 900,
        'D' => 500,
        'CD' => 400,
        'C' => 100,
        'XC' => 90,
        'L' => 50,
        'XL' => 40,
        'X' => 10,
        'IX' => 9,
        'V' => 5,
        'IV' => 4,
        'I' => 1,
    ];

    public function __construct($number)
    {
        $this->number = $number;
    }

    public function convert()
    {
        if ($this->isRoman()) {
            $this->convertToArabic();
        } elseif ($this->isArabic()) {
            $this->convertToRoman();
        } else {
            $this->result = 'error';
        }

        return $this->result;

    }

    private function isRoman()
    {
        return preg_match('/^M{0,4}(CM|CD|D?C{0,3})(XC|XL|L?X{0,3})(IX|IV|V?I{0,3})$/', $this->number);
    }

    private function isArabic()
    {
        return is_numeric ($this->number);
    }

    private function convertToArabic()
    {
        $map = $this->map;

        $this->result = 0;
        foreach ($map as $key => $value) {
            while (strpos($this->number, $key) === 0) {
                $this->result += $value;
                $this->number = substr($this->number, strlen($key));
            }
        }
    }


    private function convertToRoman()
    {
        $map = $this->map;
        $this->result = '';
        while ($this->number > 0) {
            foreach ($map as $roman => $int) {
                if ($this->number >= $int) {
                    $this->number -= $int;
                    $this->result .= $roman;
                    break;
                }
            }
        }
    }
}
