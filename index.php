<!doctype html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport"
          content="width=device-width, user-scalable=no, initial-scale=1.0, maximum-scale=1.0, minimum-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Calculator</title>
    <script type="text/javascript" src="js/jquery-3.3.1.js"></script>
    <link rel="stylesheet" href="css/bootstrap.css">
    <script type="text/javascript" src="js/bootstrap.js"></script>
    <link rel="stylesheet" href="css/style.css">
</head>
<body>
<div class="title-wrapper d-flex flex-wrap justify-content-center">
    <div class="title">Converter</div>
    <div class="description">You can easily convert numbers in this application </div>
</div>
<div class="main-container">
    <div class="main-wrapper d-flex flex-wrap justify-content-between">
        <form class="convert-form d-flex flex-wrap justify-content-between" action="" method="post">
            <textarea class="area roman-field" name="romanian" id="romanian" cols="30" rows="10"></textarea>
            <button class="submit"  id="submit" type="submit">Convert</button>
            <textarea class="area" name="arab" id="arab" cols="30" rows="10"></textarea>
        </form>
        <div class="error"></div>
    </div>
</div>
<script>
    $(document).ready(function() {
        $(".convert-form").on("submit", function(e) {
            e.preventDefault(e);
            var romanian = $("#romanian").val();
            $.ajax({
                url: "handler.php",
                data: {rom: romanian},
                type: "POST",
                success: function (response) {
                    response = JSON.parse(response);
                    if (response !== 'error'){
                        $('#arab').val(response);
                    } else {
                        $('.error').text('Wrong input');
                    }
                },
            })
        })
    })
</script>
</body>
</html>